<?php

/*
 * Decision Tree Learning
 * Problem Solving: Predicting the output of instance (OR Function)
 */
namespace Jincongho\DecisionTree;
require("vendor/Jincongho/DecisionTree/src/DecisionTree.php");
require("vendor/Jincongho/DecisionTree/src/TreeLabel.php");
require("vendor/Jincongho/DecisionTree/src/TreeNode.php");
/*
$training_set = array(
	array(array('Email', 'Infrastructure', 1), 1),
	array(array('Email', 'Programming', 1), 1),
	array(array('Email', 'Programming', 2), 1),
	array(array('Email', 'Programming', 1), 1),
	array(array('Email', 'Programming', 2), 1),
	array(array('Email', 'Infrastructure', 4), 0),
	array(array('Telephone', 'Infrastructure', 4), 0),
	array(array('Telephone', 'Programming', 4), 0),
	array(array('Line', 'Infrastructure', 1), 1),
	array(array('Line', 'Programming', 4), 0),
);
*/

$fp = fopen("WA_Fn-UseC_-HR-Employee-Attrition.csv","r");
$i = 0;
while(!feof($fp)){
	$arr_line = fgetcsv($fp);
	$i++;
	if($i == 1){
		//header
		$arr_header = $arr_line;
		$arr_header[0] = "Age";
		//var_dump($arr_header);
	}else{
		$attrition = $arr_line[1];
		unset($arr_line[1]);
		$arrTrain[] = array($arr_line,$attrition); //Attrition
	}
}
//var_dump($arrTrain);
$training_set = $arrTrain;

fclose($fp);

$orTree = new DecisionTree;
$orTree->setAttrNum(34)->addTrainingSet($training_set)->startTraining();

//print_r($orTree->getTree());

//echo "\nInformation Gain per Attribute(column#, ordered by the ascending): \n";
//var_dump();
$output = $orTree->getGain();
foreach($output as $k => $v){
	echo  "{$arr_header[$k]},$v\n";
}
//echo "\nClassify: (0, n, f) is ", ($orTree->classify(array(0, 'n', 'f')));
?>
</pre>
