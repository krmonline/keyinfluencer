<?php
require("vendor/autoload.php");
use MathPHP\InformationTheory\Entropy;
use MathPHP\Functions\Map;


function getP($x){
  $sum = 0;
  foreach($x as $v )
      $sum += $v;
  $results   = Map\Single::divide($x, $sum);
  return $results;
}


$x = array(2,3);
$p = getP($x);
$bits  = Entropy::shannonEntropy($p);
var_dump($bits);
 ?>
